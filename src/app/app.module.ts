import { ApiService } from './api.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RecipeDataService } from './recipe-data.service';
import { RecipeListHeaderComponent } from './recipe-list-header/recipe-list-header.component';
import { RecipeListComponent } from './recipe-list/recipe-list.component';
import { FormsModule } from '@angular/forms';
import { RecipeListItemComponent } from './recipe-list-item/recipe-list-item.component';
import { RecipeListFooterComponent } from './recipe-list-footer/recipe-list-footer.component';
import { IngredientListComponent } from './ingredient-list/ingredient-list.component';
import { IngredientListItemComponent } from './ingredient-list-item/ingredient-list-item.component';
import { ProductionListComponent } from './production-list/production-list.component';
import { ProductionListItemComponent } from './production-list-item/production-list-item.component';

@NgModule({
  declarations: [
    AppComponent,
    RecipeListHeaderComponent,
    RecipeListComponent,
    RecipeListItemComponent,
    RecipeListFooterComponent,
    IngredientListComponent,
    IngredientListItemComponent,
    ProductionListComponent,
    ProductionListItemComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    FormsModule
  ],
  providers: [RecipeDataService, ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
