import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-ingredient-list-item',
  templateUrl: './ingredient-list-item.component.html',
  styleUrls: ['./ingredient-list-item.component.css']
})
export class IngredientListItemComponent {

  @Input()
  ingredient: string;

  @Input() recipe: Recipe;

  @Output()
  remove: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  removeIngredient(ingredient: string) {
    this.remove.emit(ingredient);
  }
 


}
