export class Recipe {
    id: number;
    title: string = '';
    image: string = '';
    description: string = '';
    ingredient: string[];
    production: Array<any> = [];

    constructor(values: Object = {}){
        Object.assign(this, values);
    }
}
