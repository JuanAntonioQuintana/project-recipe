import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeListHeaderComponent } from './recipe-list-header.component';

describe('RecipeListHeaderComponent', () => {
  let component: RecipeListHeaderComponent;
  let fixture: ComponentFixture<RecipeListHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeListHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeListHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
