import { Component, Output, EventEmitter, Input } from '@angular/core';
import { Recipe } from '../recipe';
import { toBase64String } from '@angular/compiler/src/output/source_map';
import { $ } from 'protractor';

@Component({
  selector: 'app-recipe-list-header',
  templateUrl: './recipe-list-header.component.html',
  styleUrls: ['./recipe-list-header.component.css']
})
export class RecipeListHeaderComponent {
  newRecipe: Recipe = new Recipe();
  //selectedFile = null;
  selectedFile = null;
  ingredient: string;
  productionTitle: string;
  productionStep: string;
  ingredients: string[] = [];
  productions: Array<any> = [];
  index: number;
  numberOfSteps: number;
  finalProductions: Array<any> = [];
  buttonDisabled: boolean;
  imageRecipe: {};
  
  @Input()
  recipes: Recipe[];
  
  @Output()
  add: EventEmitter<Recipe> = new EventEmitter();

  constructor() { 
    console.log(this.ingredient);
  }

  addRecipe(){
    this.add.emit(this.newRecipe);
    this.newRecipe = new Recipe();
  }
  
  async onFileSelected(event){
    this.selectedFile = event.target.files[0];
    console.log(event);

    const getBase64 = (file) => {
      return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });
    }
    
    console.log(this.selectedFile);
    var result = await getBase64(this.selectedFile);
    console.log(result);
    this.imageRecipe = result;
    this.newRecipe.image = "" +result;
  }

  onUpload() {
    this.addRecipe();
    this.ingredients = [];
    this.finalProductions = [];
  }

  addIngredient(){
    this.ingredients.push(this.ingredient);
    this.newRecipe.ingredient = this.ingredients;
    console.log(this.ingredients);
  }

  onRemoveIngredient(ingredient){
    console.log(ingredient);
    var index = this.ingredients.indexOf(ingredient);
    this.ingredients.splice(index, 1);
  }

  onRemoveStep(step){
    console.log(step);
    var index = this.productions.findIndex(element => element.title === this.productionTitle);
    console.log(this.productions[index].steps);
    var indexElementSelected = this.productions[index].steps.indexOf(step);
    console.log("This is the position of element selected:" + indexElementSelected)
    this.productions[index].steps.splice(indexElementSelected, 1);
    console.log(this.productions[index].steps);
  }

  addProductionTitle(){
    this.buttonDisabled = true;
    this.productions.push(
      {
        title:this.productionTitle,
        steps: [
        ]
      }
    );
    var index = this.productions.findIndex(element => element.title === this.productionTitle);
    this.index = index;
    console.log("Esto es index:" + this.index);
    console.log(this.productions[index].title);
    this.numberOfSteps = this.NumberOfStepsFromProductions();
    console.log(this.numberOfSteps);
    
  }

  NumberOfStepsFromProductions(){
    return this.productions[this.index].steps.length;
  }
  addProductionStep(){
    var index = this.productions.findIndex(element => element.title === this.productionTitle);
    this.productions[index].steps.push(this.productionStep);
    console.log(this.productions);
    this.numberOfSteps = this.NumberOfStepsFromProductions();
    console.log(this.numberOfSteps);
  }

  createProduction(){
    this.buttonDisabled = false;
    this.finalProductions = this.finalProductions.concat(this.productions);
    console.log(this.finalProductions);
    this.productions = [];
    this.newRecipe.production = this.finalProductions;
    console.log("production f:" + this.newRecipe.production);
    console.log(this.newRecipe);
  }

}
