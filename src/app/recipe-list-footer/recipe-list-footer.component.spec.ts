import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipeListFooterComponent } from './recipe-list-footer.component';

describe('RecipeListFooterComponent', () => {
  let component: RecipeListFooterComponent;
  let fixture: ComponentFixture<RecipeListFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipeListFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipeListFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
