import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-recipe-list-footer',
  templateUrl: './recipe-list-footer.component.html',
  styleUrls: ['./recipe-list-footer.component.css']
})
export class RecipeListFooterComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
