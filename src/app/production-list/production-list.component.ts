import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-production-list',
  templateUrl: './production-list.component.html',
  styleUrls: ['./production-list.component.css']
})
export class ProductionListComponent {

  @Input()
  productionTitle: string
  
  @Input()
  productions: Array<any> = [];

  @Output()
  remove: EventEmitter<string> = new EventEmitter();

  constructor() {
    console.log("hello world");
  }

  onRemoveStep(production: string){
    this.remove.emit(production);
  }

}
