import { Injectable } from '@angular/core';
import { Recipe } from './recipe';
import { ApiService } from './api.service';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RecipeDataService {

  constructor(private api: ApiService) { }

  addRecipe(recipe: Recipe): Observable<Recipe> {
    return this.api.createRecipe(recipe);
  }

  deleteRecipeById(recipeId: number): Observable<Recipe> {
    return this.api.deleteRecipeById(recipeId);
  }

  updateRecipe(recipe: Recipe): Observable<Recipe> {
    return this.api.updateRecipe(recipe);
  }

  getAllRecipes(): Observable<Recipe[]> {
    return this.api.getAllRecipes();
  }

  getRecipeById(recipeId: number): Observable<Recipe> {
    return this.api.getRecipeById(recipeId);
  }
}
