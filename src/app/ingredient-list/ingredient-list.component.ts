import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Recipe } from '../recipe';

@Component({
  selector: 'app-ingredient-list',
  templateUrl: './ingredient-list.component.html',
  styleUrls: ['./ingredient-list.component.css']
})
export class IngredientListComponent {

  @Input()
  ingredients: []

  @Input()
  recipes: Recipe[];

  @Output()
  remove: EventEmitter<string> = new EventEmitter();

  constructor() {
    console.log("hello world");
  }

  onRemoveIngredient(ingredient: string){
    this.remove.emit(ingredient);
  }


}
