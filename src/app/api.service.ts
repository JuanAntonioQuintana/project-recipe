import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Http, Response } from '@angular/http';
import { Recipe } from './recipe';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const API_URL = environment.apiUrl;

@Injectable()
export class ApiService {

  constructor(private http: Http) { }

  public getAllRecipes(): Observable<Recipe[]> {
    return this.http.get(API_URL + '/recipes').map(response => {
      const recipes = response.json();
      return recipes.map((recipe) => new Recipe(recipe));
    })
    .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

  public createRecipe(recipe: Recipe): Observable<Recipe>{
    return this.http.post(API_URL + '/recipes', recipe)
    .map(response => { return new Recipe(response.json());})
    .catch(this.handleError);
  }

  public getRecipeById(recipeId: number): Observable<Recipe>{
    return this.http.get(API_URL + '/recipes/' + recipeId)
    .map(response => {
          return new Recipe(response.json());
        }).catch(this.handleError);
  }

  public updateRecipe(recipe: Recipe): Observable<Recipe> {
    return this.http.put(API_URL + '/recipes/' + recipe.id, recipe).map(response => {
      return new Recipe(response.json());
    }).catch(this.handleError);
  }

  public deleteRecipeById(recipeId: number): Observable<null> {
    return this.http.delete(API_URL + '/recipes/' + recipeId).
    map(response => null).
    catch(this.handleError);
  }
}
