import { Recipe } from './recipe';

describe('Recipe', () => {
  it('should create an instance', () => {
    expect(new Recipe()).toBeTruthy();
  });

  it('should accept values in constructor', () => {
    let recipe = new Recipe({
      title: 'Tortilla con grelos y chorizo',
      image: 'images/tortilla',
      description: '',
      ingredient: ["5 huevos de corral grandes o 7 huevos normales.", "1 kg de patatas tipo Kennebeck"],
      production: [
        {
          title: 'Preparación de la base de la tortilla. Las patatas',
          steps: [{
            "1" : "Pelamos las patatas, las lavamos para quitar restos de suciedad y muy importante, las secamos.",
            "2" : "Cortamos en láminas semifinas, a mí no me gusta que se deshagan sino que al freírlas se tuesten un poco."
          }]
        }
      ],
    })
  })
});
