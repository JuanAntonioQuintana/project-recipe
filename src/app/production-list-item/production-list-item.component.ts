import { Component, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-production-list-item',
  templateUrl: './production-list-item.component.html',
  styleUrls: ['./production-list-item.component.css']
})
export class ProductionListItemComponent {

  @Input()
  productionTitle: string
  
  @Input()
  production: object;


  @Output()
  remove: EventEmitter<string> = new EventEmitter();

  constructor() {
  }

  removeStep(production: string) {
    this.remove.emit(production);
  }
 

}
