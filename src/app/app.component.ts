import { Component, OnInit } from '@angular/core';
import { RecipeDataService } from './recipe-data.service';
import { Recipe } from './recipe';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [RecipeDataService]
})

export class AppComponent implements OnInit{

  recipes: Recipe[] = [];
  
  constructor(private recipeDataService: RecipeDataService){

  }

  
  public ngOnInit() {
    this.recipeDataService
      .getAllRecipes()
      .subscribe(
        (recipes) => {
          this.recipes = recipes;
      });
  }

  
  onAddRecipe(recipe){
    this.recipeDataService
    .addRecipe(recipe)
    .subscribe(
      (newRecipe) => {
        this.recipes = this.recipes.concat(newRecipe);
      })
  }

  onRemoveRecipe(recipe){
    this.recipeDataService
    .deleteRecipeById(recipe.id)
    .subscribe(
      (_) => {
        this.recipes=
          this.recipes.filter((t) => t.id !== recipe.id);
      }
    );
  }
}
